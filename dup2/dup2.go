package main

import (
	"bufio"
	"fmt"
	"os"
)

//Counter type alias for [string]int
type Counter map[string]int

func main() {
	counts := make(Counter)
	files := os.Args[1:]
	if len(files) > 0 {
		for _, fn := range files {
			f, err := os.Open(fn)
			if err == nil {
				countLines(f, counts)
				f.Close()
			}
		}
	} else {
		countLines(os.Stdin, counts)
	}

	for line, n := range counts {
		fmt.Printf("%d\t%s \n", n, line)
	}

}

func countLines(f *os.File, counts Counter) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
	}

}
