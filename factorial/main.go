package main

import "fmt"

func main() {
	err := ddd()
	fmt.Println(err.Error())
}

func ddd() error {
	return fmt.Errorf("slorem ipsum")
}

func factorial(n int) int {
	if n == 0 {
		return 1
	}
	return n * factorial(n-1)
}
