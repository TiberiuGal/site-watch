package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {

	printArray(os.Args[1:])
	printForLoop(os.Args[1:])
	printForeach(os.Args[1:])
	printWithJoin(os.Args[1:])
}

func printWithJoin(args []string) {
	fmt.Println(strings.Join(args, ""))
}

func printArray(args []string) {
	fmt.Println(args)
}

func printForLoop(args []string) {
	var s, sep string
	for i := 0; i < len(args); i++ {
		s += sep + strconv.FormatInt(int64(i+1), 10) + ": " + args[i]
		sep = " "
	}
	fmt.Println(s)
}

func printForeach(args []string) {
	for _, arg := range args {
		fmt.Print(arg, " ")
	}
	fmt.Println()
}
