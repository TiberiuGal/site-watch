package main

import (
	"fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	mongoDBHost = "localhost"
	mongoDBName = "goingo"
)

type (
	// User struct
	User struct {
		ID       bson.ObjectId `bson:"_id"`
		Username string        `bson:"username"`
		Password string        `bson:"pass"`
		Email    string        `bson:"email"`
	}
)

func main() {
	fmt.Println("ok, starting")
	session, err := mgo.Dial(mongoDBHost)
	if err != nil {
		fmt.Println("error dialing ongo", err)
		return
	}
	c := session.DB(mongoDBName).C("users")
	var result User
	query := bson.M{"email": "galtiberiu@gmail.com"}

	err = c.Find(query).One(&result)
	if err != nil {
		fmt.Println("error querying", err)
		return
	}

	fmt.Println(result.Password)

}
