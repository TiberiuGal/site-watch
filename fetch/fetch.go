package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func main() {

	for _, url := range os.Args[1:] {
		if !strings.HasPrefix(url, "http") {
			url = "http://" + url
		}
		resp, err := http.Get(url)

		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			continue
		}
		fmt.Println(resp.Status)
		io.Copy(os.Stdout, resp.Body)
		/*b, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		fmt.Printf("%s", b)
		*/
	}

}
