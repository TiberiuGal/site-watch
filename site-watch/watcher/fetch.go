package watcher

import (
	"io/ioutil"
	"net/http"
)

func fetchURL(url string, callback Validator) (err error) {
	err = nil
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	err = callback(string(body))

	return
}

func handleSite(site Site) (res FetchResult) {
	res.SiteID = site.URL

	for _, validation := range site.Validation {
		err := fetchURL(site.URL, validatorFactory(validation.ValidatorType, validation.ValidationValue))
		res.CheksPerformed++
		if err != nil {
			res.ErrorData = append(res.ErrorData, err.Error())
			res.Status = 1
		} else {
			res.Passes++
		}
	}
	return
}

// HandleSites -  does the magic
func HandleSites(sites []Site, saveResultsChan chan<- FetchResult) {
	for _, site := range sites {
		res := handleSite(site)
		saveResultsChan <- res
	}
}
