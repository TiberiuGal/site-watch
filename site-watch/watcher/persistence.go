package watcher

import (
	"fmt"

	"gopkg.in/mgo.v2"
)

// Connect  to database
func Connect(host, dbName string) *mgo.Database {
	session, err := mgo.Dial(host)
	if err != nil {

		fmt.Println("cannot connect to mongodb", err)
		panic("I'll die now")

	}
	return session.DB(dbName)

}

func saveSites(sites []Site, db *mgo.Database) {
	for _, site := range sites {
		db.C("sites").Insert(site)
	}
}

// GetSites get a list of sites to check
func GetSites(db *mgo.Database) []Site {
	var sites []Site
	err := db.C("sites").Find(nil).All(&sites)
	if err != nil {
		fmt.Println("cannot fetch sites", err)
		panic("try to recover from this")
	}

	return sites
}

func saveResult(db *mgo.Database, res FetchResult) {
	db.C("fetch_results").Insert(res)
}

// WaitForResults from fetch
func WaitForResults(db *mgo.Database, ch chan FetchResult) {
	for {
		saveResult(db, <-ch)
	}
}
