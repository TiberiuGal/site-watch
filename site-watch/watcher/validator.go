package watcher

import (
	"fmt"
	"regexp"
	"strings"

	xpath "gopkg.in/xmlpath.v2"
)

type (
	// Validator generic function signature for url body validation
	Validator func(input string) error

	// ValidatorType custom type
	ValidatorType string
)

const (
	// ValidatorContains simple text contains validator type
	ValidatorContains ValidatorType = "contains"

	// ValidatorRegexp test a string agains regexp
	ValidatorRegexp ValidatorType = "regexp"

	// ValidatorXpath test an xml for a given path
	ValidatorXpath ValidatorType = "xpath"
)

func validatorFactory(validationType ValidatorType, validationValue string) Validator {
	switch validationType {
	case ValidatorContains:
		return func(input string) error {
			if !strings.Contains(input, validationValue) {
				return fmt.Errorf("string.contains validator failed for %s ", validationValue)
			}
			return nil
		}

	case ValidatorRegexp:
		return func(input string) error {
			if r, err := regexp.MatchString(validationValue, input); !r || err != nil {
				return fmt.Errorf("string.regepx validator failed for %s ", validationValue)
			}
			return nil
		}

	case ValidatorXpath:
		return func(input string) error {
			path := xpath.MustCompile(validationValue)
			root, err := xpath.ParseHTML(strings.NewReader(input))
			if err != nil {
				return err
			}
			if _, ok := path.String(root); !ok {
				return fmt.Errorf("string.xpath validator failed for %s ", validationValue)
			}
			return nil
		}

	}

	panic("undefined validation type")
}
