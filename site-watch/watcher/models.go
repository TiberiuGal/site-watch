package watcher

import "time"

type (
	// Validation information
	Validation struct {
		ValidatorType   ValidatorType `bson:"type"`
		ValidationValue string        `bson:"value"`
	}

	// Site struct
	Site struct {
		URL        string       `bson:"_id"`
		Validation []Validation `bson:"validation"`
	}

	// FetchResult struct
	FetchResult struct {
		SiteID         string `bson:"site"`
		Status         int
		CheksPerformed int      `bson:"cnt"`
		Passes         int      `bson:"pass"`
		ErrorData      []string `bson:"error_data,omitempty"`
		Date           time.Time
	}
)
