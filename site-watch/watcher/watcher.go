package watcher

import "time"

const (
	mongoDBHost = "localhost"
	mongoDBName = "site_watch"
)

func Run() {
	db := Connect(mongoDBHost, mongoDBName)
	ch := make(chan FetchResult)
	for {
		go HandleSites(GetSites(db), ch)
		go WaitForResults(db, ch)
		time.Sleep(20 * time.Second)
	}

}
