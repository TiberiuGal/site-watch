package main

import (
	"tibi/hello/site-watch/server"
	"tibi/hello/site-watch/watcher"
)

func main() {
	go watcher.Run()
	server.Run()
}
