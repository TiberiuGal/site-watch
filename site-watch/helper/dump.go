package helper

import (
	"fmt"

	sw "../watcher"
)

// VarDump dump site variables
func VarDump(site sw.Site) {
	fmt.Println("site", site)
}
